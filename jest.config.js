/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-require-imports */
const { pathsToModuleNameMapper } = require('ts-jest/utils');
const { compilerOptions } = require('./tsconfig');

module.exports = {
	preset: 'jest-preset-angular',
	roots: ['<rootDir>/src/'],
	testMatch: ['**/+(*.)+(spec).+(ts)'],
	setupFilesAfterEnv: ['<rootDir>/test.ts'],
	collectCoverage: true,
	coverageReporters: ['html'],
	coverageDirectory: 'coverage/mma-ux',
	moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths || {}, {
		prefix: '<rootDir>/'
	})
};