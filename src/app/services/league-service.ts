import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { IMember, IEvent, ILeague } from '../+state/types/app-state';
import { environment } from 'src/environments/environment';

@Injectable()
export class LeagueService {
	private readonly environment = environment;

	private requestOptions = {
		withCredentials: true
	};

	constructor(
		private http: HttpClient,
	) { }

	public getLeagueByTeam(teamId: string): Observable<any> {
		return this.http.get(`http://${this.environment.baseUrl}/leagues/containing-team/${teamId}`, this.requestOptions);
	}

	public getLeagueById(leagueId: string): Observable<any> {
		return this.http.get(`http://${this.environment.baseUrl}/leagues/${leagueId}`, this.requestOptions);
	}

	public getEvent(eventId: string): Observable<any> {
		return this.http.get(`http://${this.environment.baseUrl}/events/${eventId}`, this.requestOptions);
	}

	public updateEvent(currentEvent: IEvent, teams: IMember[]): Observable<any> {
		return this.http.post(`http://${this.environment.baseUrl}/events/update`, { currentEvent, teams }, this.requestOptions);
	}

	public getTeam(teamId: string): Observable<any> {
		return this.http.get(`http://${this.environment.baseUrl}/teams/${teamId}`, this.requestOptions);
	}

	public getAllTeams(leaguId: string): Observable<any> {
		return this.http.get(`http://${this.environment.baseUrl}/teams/all/${leaguId}`, this.requestOptions);
	}

	public udpateTeam(team: IMember): Observable<any> {
		return this.http.post(`http://${this.environment.baseUrl}/teams/update/${team.id}`, team, this.requestOptions);
	}

	public createNewEvent(event: IEvent, league: ILeague): Observable<any> {
		return this.http.post(`http://${this.environment.baseUrl}/events/new`, { event, league}, this.requestOptions);
	}

	public getEventsList(year: string): Observable<any> {
		return this.http.get(`http://${this.environment.baseUrl}/events/get-events-list/${year}`, this.requestOptions);
	}

	public addMatchups(matchups: any[], league: ILeague, event: string): Observable<any> {
		return this.http.post(`http://${this.environment.baseUrl}/leagues/add-match-ups`, { matchups, league, event}, this.requestOptions);
	}
}
