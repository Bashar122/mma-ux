import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from 'src/environments/environment';

@Injectable()
export class LoginService {
	private readonly environment = environment;

	constructor(
		private http: HttpClient,
	) { }

	public login(credentials): Observable<any> {

		const httpOptions = {
			withCredentials: true,
			'Access-Control-Allow-Origin': '0.0.0.0',
		};

		return this.http.post(`http://${this.environment.baseUrl}/auth/login`, credentials, httpOptions);
	}

	public verifyCredentials(): Observable<any> {
		const requestOptions = {
			withCredentials: true,
			'Access-Control-Allow-Origin': '0.0.0.0',
		};

		return this.http.get(`http://${this.environment.baseUrl}/auth/verify`, requestOptions);
	}
}
