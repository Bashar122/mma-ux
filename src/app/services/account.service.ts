import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'src/environments/environment';

@Injectable()
export class AccountService {
	public httpOptions = {
		withCredentials: true,
	};

	private readonly environment = environment;

	constructor(
		private http: HttpClient,
	) { }


	public getUserInfo(id: string): Observable<any> {
		return this.http.get(`http://${this.environment.baseUrl}/account/${id}`, this.httpOptions);
	}

	public updatePassword(password: string, id: any): Observable<any> {
		return this.http.post(`http://${this.environment.baseUrl}/account/update-password/${id}`, { password }, this.httpOptions);
	}
}
