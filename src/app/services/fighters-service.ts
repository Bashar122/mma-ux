import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'src/environments/environment';

@Injectable()
export class FightersService {
	private readonly environment = environment;

	constructor(
		private http: HttpClient
	) { }


	public getAllFighters(): Observable<any> {
		return this.http.get(`http://${this.environment.baseUrl}/fighters/allFighters`);
	}

	public getNewFighter(path: string): Observable<any> {
		return this.http.post(`http://${this.environment.baseUrl}/fighters/getNewFighter`, { path });
	}
}
