import { Injectable } from '@angular/core';
import { IPick, IBonus, SCORING_SYSTEM_ENUM } from '../+state/types/app-state';

@Injectable()
export class ScoringUtil {
	public scoreMethod(result: IPick, pick: IPick): number {
		let matchPoints = 0;

		if (result.winner === pick.winner) {
			if (pick.winner === 'draw') {
				return 400;
			}

			matchPoints += 200;
		} else {
			return 0;
		}

		if (result.method === pick.method) {
			matchPoints += 75;
		}

		if (pick.method !== 'decision' && result.round === pick.round) {
			matchPoints += 50;
		}

		for (const bonus of result.fight.bonus) {
			if (bonus.recipient === 'both' || bonus.recipient === pick.winner) {
				matchPoints += bonus.amount;
			}
		}

		return matchPoints;
	}

	public scoreConfidence(result: IPick, pick: IPick): number {
		if (result.winner === pick.winner) {
			return pick.confidence;
		}

		return 0;
	}

	public potentialScore(pick: IPick, fight: any, scoringType: string): number {
		if (!pick?.winner) {
			return 0;
		}

		if (scoringType === SCORING_SYSTEM_ENUM.CONFIDENCE) {
			return pick.confidence || 0;
		}

		let matchPoints = 0;

		if (pick?.winner === 'draw') {
			return 400;
		}

		// maximium potential winner score
		matchPoints += 200;

		// maximium potential method score
		matchPoints += 75;

		// maximium potential round score
		if (pick?.method !== 'decision') {
			matchPoints += 50;
		}

		// maximium potential bonuses
		for (const bonus of fight.bonus) {
			if (bonus.recipient === 'both' || bonus.recipient === pick?.winner) {
				matchPoints += bonus.amount;
			}
		}

		// maximium potential points earned 
		return matchPoints;
	}

	public getBonusPts(fighterId: string, fight: any): IBonus[] {
		let bonusPts = [];

		for (const bonus of fight.bonus) {
			if (bonus.recipient === fighterId || bonus.recipient === 'both') {
				bonusPts.push(bonus);
			}
		}

		return bonusPts;
	}
}
