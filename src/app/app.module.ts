import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';
import { AppFacade } from './+state/app.facade';
import { AppEffects } from './+state/app.effects';
import { reducer, initialState as appInitialState } from './+state/app.reducer';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginService, LeagueService } from './services';
import { HeaderComponent } from './shared-components/header/header.component';
import { LeagueListingComponent } from './shared-components/league-listing/league-listing.component';
import { LoginComponent } from './pages/login/login.component';
import { LeagueHomeComponent } from './pages/league-home/league-home.component';
import { TeamPageComponent } from './pages/team-page/team-page.component';
import { FilterPipe } from './pipes/filter';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ScoringUtil } from './helpers/scoring-util';
import { MatchUpComponent } from './pages/match-up/match-up.component';
import { FighterHistoryComponent } from './shared-components/fighter-history/fighter-history.component';
import { EditFightersComponent } from './pages/edit-fighters/edit-fighters.component';
import { FightersService } from './services/fighters-service';
import { AccountService } from './services/account.service';
import { LeagueMatchUpComponent } from './shared-components/league-matchup/league-matchup.component';
import { CommissionerToolsComponent } from './pages/commissioner-tools/commissioner-tools.component';
import { LoadingModalComponent } from './shared-components/loading-modal/loading-modal.component';
import { CustomCheckboxesComponent } from './shared-components/custom-inputs/custom-checkboxes/custom-checkboxes.component';
import { CustomDropdownsComponent } from './shared-components/custom-inputs/custom-dropdowns/custom-dropdowns.component';
import { UserSettingsComponent } from './pages/user-settings/user-settings.component';
import { CreateEventComponent } from './pages/create-event/create-event.component';
import { DropdownFilterPipe } from './pipes/dropdown-filter.pipe';
import { CustomSearchComponent } from './shared-components/custom-inputs/custom-search/custom-search.component';

const appRoutes: Routes = [
  { path: 'home-page', component: HomePageComponent },
  { path: 'league-home', component: LeagueHomeComponent },
  { path: 'team-page', component: TeamPageComponent },
  { path: 'match-up', component: MatchUpComponent },
  { path: 'edit-fighters', component: EditFightersComponent },
  { path: 'commish-tools', component: CommissionerToolsComponent },
  { path: 'user-settings', component: UserSettingsComponent },
  { path: 'create-event', component: CreateEventComponent },
  { path: '**', redirectTo: '/home-page', pathMatch: 'full' },
];

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    CreateEventComponent,
    DropdownFilterPipe,
    HomePageComponent,
    TeamPageComponent,
    HeaderComponent,
    LoginComponent,
    LeagueHomeComponent,
    LeagueListingComponent,
    FilterPipe,
    MatchUpComponent,
    FighterHistoryComponent,
    EditFightersComponent,
    LeagueMatchUpComponent,
    CommissionerToolsComponent,
    LoadingModalComponent,
    CustomCheckboxesComponent,
    CustomDropdownsComponent,
    UserSettingsComponent,
    CustomSearchComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    StoreModule.forRoot(
      { app: reducer },
      {
        initialState: { app: appInitialState },
        runtimeChecks: {
          strictStateImmutability: false,
          strictActionImmutability: false,
          strictStateSerializability: true,
          strictActionSerializability: true,
          strictActionWithinNgZone: true,
        },
      },
    ),
    StoreDevtoolsModule.instrument({
      maxAge: 20,
    }),
    EffectsModule.forRoot([AppEffects]),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    AccountService,
    AppFacade,
    LoginService,
    LeagueService,
    ScoringUtil,
    FightersService,
  ],
})
export class AppModule {}
