import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'dropdownFilter',
})
export class DropdownFilterPipe implements PipeTransform {
	public transform(items: any[], filter: string): any {
		if (!items || !filter) {
			return items;
		}

		return items.filter(item => item.display.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
	}
}