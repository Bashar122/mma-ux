import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'filter',
	pure: false
})
export class FilterPipe implements PipeTransform {
	public transform(items: any[], filter: Object): any {
		if (items && filter === 'ascending') {
			return this.ascending(items);
		} else if (items && filter === 'descending') {
			return this.descending(items);
		} else if (items && filter === 'decendingByIndex') {
			return this.decendingByIndex(items);
		}
	}

	private ascending(items): any {
		items.sort((a, b) => {
			if (b.record && a.record && b.record.wins - a.record.wins) {
				return b.record.wins - a.record.wins;
			} else if (b.record && a.record && b.record.wins === a.record.wins) {
				if (b.record.losses === a.record.losses) {
					return b.totalPoints - a.totalPoints;
				}

				return a.record.losses - b.record.losses;
			}
		});

		return items;
	}

	private descending(items): any {
		items.sort((a, b) => {
			if (b.record && a.record && a.record.wins - b.record.wins) {
				return a.record.wins - b.record.wins;
			} else if (b.record && a.record && a.record.wins === b.record.wins) {
				if (a.record.losses === b.record.losses) {
					return a.totalPoints - b.totalPoints;
				}

				return b.record.losses - a.record.losses;
			}
		});

		return items;
	}

	private decendingByIndex(items): any {
		return items.map((item, index, allItems) => {
			return allItems[allItems.length - 1 - index];
		});
	}
}
