import { Component, Input } from '@angular/core';
import { ScoringUtil } from 'src/app/helpers/scoring-util';

@Component({
	selector: 'league-matchup',
	styleUrls: ['league-matchup.component.scss'],
	templateUrl: './league-matchup.component.html',
})
export class LeagueMatchUpComponent {
	// TODO: make type
	@Input()
	public team1: any;

	@Input()
	public team2: any;

	@Input()
	public selectedWeek: number;

	private _event;

	@Input()
	public set event(value) {
		this.calculateScore(value);
		this._event = value;
	}

	public get event(): any {
		return this._event;
	}


	constructor(
		private scorUtil: ScoringUtil,
	) { }

	public calculateScore(event): void {
		if (event) {
			let fightCounter = 1;
			this.team1.picks[this.selectedWeek].totalPoints = 0;
			this.team2.picks[this.selectedWeek].totalPoints = 0;

			for (const pick of event.lineup) {
				this.team1.picks[this.selectedWeek].totalPoints += this.scorUtil.scoreMethod(
					pick,
					this.team1.picks[this.selectedWeek].picks[fightCounter - 1],
				);

				this.team2.picks[this.selectedWeek].totalPoints += this.scorUtil.scoreMethod(
					pick,
					this.team2.picks[this.selectedWeek].picks[fightCounter - 1],
				);

				fightCounter++;
			}
		}
	}
}
