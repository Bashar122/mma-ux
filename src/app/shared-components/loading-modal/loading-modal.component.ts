import { Component, Input } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
	selector: 'loading-modal',
	styleUrls: ['loading-modal.component.scss'],
	templateUrl: 'loading-modal.component.html',
	animations: [
		trigger('dialog', [
			transition('void => *', [
				style({ transform: 'scale3d(.3, .3, .3)' }),
				animate(100)
			]),
			transition('* => void', [
				animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
			])
		]),
	]
})
export class LoadingModalComponent {
	@Input()
	public isLoading: boolean = false;

	@Input()
	public text: string;
}
