import { Component, Input, OnInit } from '@angular/core';
import { ControlContainer, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
	selector: 'search',
	styleUrls: ['custom-search.component.scss'],
	templateUrl: 'custom-search.component.html',
})
export class CustomSearchComponent implements OnInit {
	@Input()
	public options: any[];

	@Input()
	public controlName: string;

	public originalFormGroup;
	public dropdownFlag: boolean = true;

	public filterForm: FormGroup = this.fb.group({
		filter: ['', Validators.required],
	});

	constructor(
		public readonly controlContainer: ControlContainer,
		private readonly fb: FormBuilder,
	) {}

	public ngOnInit(): void {
		this.originalFormGroup = this.controlContainer.control;
	}

	public onInputChange(): void {
		if (this.filterForm.get('filter').value) {
			this.dropdownFlag = true;
		}
	}

	public selectOption(option): void {
		if (option.value) {
			this.filterForm.get('filter').setValue(option.display);
			this.originalFormGroup.controls[this.controlName].setValue(option.value);
			this.dropdownFlag = false;
		}
	}
}
