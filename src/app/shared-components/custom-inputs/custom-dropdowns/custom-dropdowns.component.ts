import { Component, Input, OnInit } from '@angular/core';
import { ControlContainer, FormGroup, FormBuilder } from '@angular/forms';

@Component({
	selector: 'dropdown',
	styleUrls: ['custom-dropdowns.component.scss'],
	templateUrl: 'custom-dropdowns.component.html',
})
export class CustomDropdownsComponent implements OnInit {
	@Input()
	public options: any[];

	@Input()
	public controlName: string;

	@Input()
	public disabled: boolean;

	public filterForm: FormGroup = this.fb.group({
		filter: [''],
	});

	constructor(
		public controlContainer: ControlContainer,
		private readonly fb: FormBuilder,
	) {}

	public ngOnInit(): void {
		if (this.disabled) {
			this.controlContainer.control.disable();
		}
	}
}
