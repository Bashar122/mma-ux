import { Component, Input, OnInit } from '@angular/core';
import { ControlContainer } from '@angular/forms';

@Component({
	selector: 'checkbox',
	styleUrls: ['custom-checkboxes.component.scss'],
	templateUrl: 'custom-checkboxes.component.html',
})
export class CustomCheckboxesComponent implements OnInit {
	@Input()
	public items: any;

	@Input()
	public controlName: string;

	@Input()
	public customStyles;

	@Input()
	public prepopProperty: string;

	@Input()
	public disabled: string;

	public originalFormGroup;
	public selected: string;

	constructor(
		public controlContainer: ControlContainer,
	) { }

	public ngOnInit(): void {
		this.originalFormGroup = this.controlContainer.control;
		this.selected = this.originalFormGroup.value[this.prepopProperty];
	}

	public setSelected(selected: string): void {
		if (!this.disabled) {
			this.selected = selected;
		}
	}
}
