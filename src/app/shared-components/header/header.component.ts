import { Component, OnDestroy } from '@angular/core';
import { AppFacade } from '../../+state/app.facade';
import { IUserInfo, ILeague } from '../../+state/types/app-state';
import { Subscription } from 'rxjs';

@Component({
	selector: 'navigation',
	styleUrls: ['./header.component.scss'],
	templateUrl: './header.component.html',
})
export class HeaderComponent implements OnDestroy {
	public userInfo: IUserInfo;
	public isLoggedIn: boolean = false;
	public hideLeagueNav: boolean;
	public mobileNavOpened: boolean = false;
	public isCommissioner: boolean = false;

	private getUserInfo$: Subscription;
	private getLeague$: Subscription;
	private getIsCommissioner$: Subscription;

	constructor(
		private appFacade: AppFacade,
	) {
		this.getUserInfo$ = this.appFacade.getUserInfo$.subscribe((userInfo: IUserInfo) => {
			if (userInfo) {
				this.isLoggedIn = true;
				this.userInfo = userInfo;
			}
		});

		this.getLeague$ = this.appFacade.getLeague$.subscribe((league: ILeague) => {
			this.hideLeagueNav = league ? true : false;
		});

		this.getIsCommissioner$ = this.appFacade.isCommissioner$.subscribe((isCommissioner: boolean) => {
			this.isCommissioner = isCommissioner;
		});
		
		this.appFacade.verifyCredentials();
	}

	public toggleMobileNav(): void {
		this.mobileNavOpened = !this.mobileNavOpened;
	}

	public ngOnDestroy(): void {
		if (this.getUserInfo$) {
			this.getUserInfo$.unsubscribe();
		}

		if (this.getLeague$) {
			this.getLeague$.unsubscribe();
		}

		if (this.getIsCommissioner$) {
			this.getIsCommissioner$.unsubscribe();
		}
	}
}
