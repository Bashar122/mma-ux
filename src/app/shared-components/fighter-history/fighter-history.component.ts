import { Component, Input } from '@angular/core';

@Component({
	selector: 'fighter-history',
	styleUrls: ['fighter-history.component.scss'],
	templateUrl: 'fighter-history.component.html',
})
export class FighterHistoryComponent {
	@Input()
	public fighter: any;

	public collapseFight: number[] = [];

	public toggleFight(index): void {
		if (!this.collapseFight.includes(index)) {
			this.collapseFight.push(index);
		} else {
			this.collapseFight = this.collapseFight.filter((value) => value !== index);
		}
	}
}
