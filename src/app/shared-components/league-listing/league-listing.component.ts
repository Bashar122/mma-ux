import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IMember } from 'src/app/+state/types/app-state';

@Component({
	selector: 'league-listing',
	styleUrls: ['./league-listing.component.scss'],
	templateUrl: './league-listing.component.html',
})
export class LeagueListingComponent {
	@Input()
	public team: any;

	@Output()
	public selectedTeam: EventEmitter<IMember> = new EventEmitter<IMember>();

	public onSelectedLeague(team: IMember): void {
		this.selectedTeam.emit(team);
	}
}
