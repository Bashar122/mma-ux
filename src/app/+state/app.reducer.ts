import { createReducer, on } from '@ngrx/store';
import * as AppActions from './app.actions';
import * as AppTypes from './types/app-state';

export const APP_FEATURE_KEY = 'app';

export interface IAppPartialState {
	readonly [APP_FEATURE_KEY]: IAppState;
}

export interface IAppState {
	error?: string;
	event?: AppTypes.IEvent;
	league?: AppTypes.ILeague;
	selectedTeam?: AppTypes.IMember;
	userInfo?: AppTypes.IUserInfo;
	isCommissioner?: boolean;
	selectedWeek?: number;
	allFighters?: any[];
	events?: any[];
}

export const initialState: IAppState = {};

export const reducer = createReducer(
	initialState,
	on(AppActions.setUserInfo, (state, userInfo) => {
		return {
			...state,
			userInfo,
		};
	}),
	on(AppActions.setLeague, (state, { league, isCommissioner }) => {
		if (league) {
			return {
				...state,
				isCommissioner,
				league,
			};
		}
	}),
	on(AppActions.setTeam, (state, { team }) => {
		return {
			...state,
			selectedTeam: team,
		};
	}),
	on(AppActions.setEvent, (state, event) => {
		return {
			...state,
			event,
		};
	}),
	on(AppActions.printError, (state, { error }) => {
		return {
			...state,
			error,
		};
	}), 
	on(AppActions.setSelectedWeek, (state, { selectedWeek }) => {
		return {
			...state,
			selectedWeek,
		};
	}),
	on(AppActions.setAllFighters, (state, { allFighters }) => {
		return {
			...state,
			allFighters,
		};
	}),
	on(AppActions.setEventsList,  (state, { events }) => {
		return {
			...state,
			events,
		};
	}),
);
