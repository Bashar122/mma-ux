import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, Store, select } from '@ngrx/store';
import { of, Observable } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import * as AppActions from './app.actions';
import * as AppSelectors from './app.selectors';
import { LoginService, LeagueService, AccountService } from '../services';
import { IAppState } from './app.reducer';
import { FightersService } from '../services/fighters-service';

@Injectable()
export class AppEffects {

	public verifyCredentials$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.verifyCredentials),
		switchMap(() =>
			this.loginService.verifyCredentials()
				.pipe(
					map((response: any) => {
						return AppActions.setUserInfo(response);
					}),
					catchError((response) => {
						console.log('Error Verifying If User Is Logged In: ', response.error.message);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	),
	);

	public login$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.login),
		switchMap(action =>
			this.loginService.login({ username: action.username, password: action.password })
				.pipe(
					map((response: any) => {
						if (response) {
							return AppActions.setUserInfo(response);
						}
					}),
					catchError((response) => {
						console.log('Login Error: ', response.error.message);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	));

	public getUserInfo$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.getUserInfo),
		switchMap(action =>
			this.accountService.getUserInfo(action.id)
				.pipe(
					map((response: any) => {
						if (response) {
							return AppActions.setUserInfo(response);
						}
					}),
					catchError((response) => {
						console.log('Getting User Info Error: ', response.error.message);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	));



	public fetchLeague$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.fetchLeague),
		switchMap(action =>
			this.leagueService.getLeagueById(action.leagueId)
				.pipe(
					map((response: any) => {
						if (response) {
							// TODO: look into whether this needs to stay defaulted to false for commissioner
							return AppActions.setLeague(response, false);
						}
					}),
					catchError((response) => {
						console.log('Retrieving League Error: ', response.error.message);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	),
	);

	public getLeagueByTeam$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.getLeagueByTeam),
		switchMap(action =>
			this.leagueService.getLeagueByTeam(action.teamId)
				.pipe(
					map((response: any) => {
						if (response) {
							const isCommissioner: boolean = this.isCommissioner(action.teamId, response.configuration.commissioners);
							return AppActions.setLeague(response, isCommissioner);
						}
					}),
					catchError((response: any) => {
						console.log('Retrieving League Error: ', response.error.message);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	),
	);

	public udpateTeam$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.udpateTeam),
		switchMap(action =>
			this.leagueService.udpateTeam(action.team)
				.pipe(
					map((returnedTeam) => {
						if (returnedTeam) {
							return AppActions.setTeam(returnedTeam);
						}
					}),
					catchError((response) => {
						console.log('Updating Picks Error: ', response.error.message);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	),
	);

	public getEvent$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.getEvent),
		switchMap(action =>
			this.leagueService.getEvent(action.eventId)
				.pipe(
					map((response: any) => {
						if (response) {
							return AppActions.setEvent(response.event);
						}
					}),
					catchError((response) => {
						console.log('Error retrieving event: ', response.errror.message);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	),
	);

	public updateEvent$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.updateEvent),
		switchMap(action =>
			this.leagueService.updateEvent(action.event, action.teams)
				.pipe(
					map((response: any) => {
						if (response) {
							return AppActions.setEvent(response.event);
						}
					}),
					catchError((response) => {
						console.log('Error updating event: ', response.error.message);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	),
	);

	public updatePassword$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.updatePassword),
		withLatestFrom(this.store.pipe(select(AppSelectors.getUserInfo))),
		switchMap(([action, userInfo]) =>
			this.accountService.updatePassword(action.password, userInfo.id)
				.pipe(
					map((response: any) => {
						if (response) {
							return AppActions.printError(response.message);
						}
					}),
					catchError((response) => {
						console.log('Error updating password: ', response);
						return of(AppActions.printError(response.error.message));
					}),
				)),
	),
	);

	public getNewFighter$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.getNewFighter),
		switchMap(action =>
			this.fightersService.getNewFighter(action.path)
				.pipe(
					map((response: any) => {
						if (response) {
							return AppActions.printError(response.message);
						}
					}),
					catchError((response) => {
						console.log('Error getting new fighter: ', response);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	),
	);

	public getAllFighters$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.fetchAllFighters),
		switchMap(() =>
			this.fightersService.getAllFighters()
				.pipe(
					map((response: any) => {
						if (response) {
							return AppActions.setAllFighters(response);
						}
					}),
					catchError((response) => {
						console.log('Error getting all fighters: ', response);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	),
	);

	public createNewEvent$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.createNewEvent),
		switchMap(action => 
			this.leagueService.createNewEvent(action.event, action.league)
				.pipe(
					map((response:any) => {
						if (response) {
							return AppActions.printError(response);
						}
					}),
					catchError((response) => {
						console.log('Error creating new event: ', response);
						return of(AppActions.printError(response.error.message));
					}),
				),
		),
	),
	);

	public getEventsList$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.retrieveEventsList),
		switchMap(action => 
			this.leagueService.getEventsList(action.year)
				.pipe(
					map((response: any) => {
						if (response) {
							return AppActions.setEventsList(response.events);
						}
					}),
					catchError((response) => {
						console.log('Error getting events list: ', response);
						return of(AppActions.printError(response.error.message));
					}),
				))
	));

	public addMatchups$: Observable<Action> = createEffect(() => this.actions$.pipe(
		ofType(AppActions.addMatchups),
		switchMap(action => 
			this.leagueService.addMatchups(action.matchups, action.league, action.event)
				.pipe(
					map((response: any) => {
						if (response) {
							return AppActions.printError('match up added');
						}
					}),
					catchError((response) => {
						console.log('Error addings matchups: ', response);
						return of(AppActions.printError(response.error.message));
					}),
				))
	));

	constructor(
		private readonly actions$: Actions,
		private readonly accountService: AccountService,
		private readonly fightersService: FightersService,
		private readonly loginService: LoginService,
		private readonly leagueService: LeagueService,
		private readonly store: Store<IAppState>
	) { }

	private isCommissioner(teamId: string, leagueCommissioners: string[]): boolean {
		for (const commissioner of leagueCommissioners) {
			if (teamId === commissioner) {
				return true;
			}
		}

		return false;
	}
}

