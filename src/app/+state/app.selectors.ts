import { createFeatureSelector, createSelector } from '@ngrx/store';
import { APP_FEATURE_KEY, IAppState } from './app.reducer';
import { ILeague } from './types';

export const getAppState = createFeatureSelector<IAppState>(APP_FEATURE_KEY);

export const getUserInfo = createSelector(
	getAppState,
	(state: IAppState) => {
		if (state?.userInfo) {
			return state.userInfo;
		}
	},
);

export const getEvent = createSelector(
	getAppState,
	(state: IAppState) => {
		if (state?.event) {
			return state.event;
		}
	},
);

export const getLeague = createSelector(
	getAppState,
	(state: IAppState) => {
		if (state?.league) {
			return state.league;
		}
	},
);

export const getTeams = createSelector(
	getLeague,
	(league: ILeague) => {
		if (league?.teams) {
			return league.teams;
		}
	},
);

export const getSelectedTeam = createSelector(
	getAppState,
	(state: IAppState) => {
		if (state?.selectedTeam) {
			return state.selectedTeam;
		}
	}
);

export const getError = createSelector(
	getAppState,
	(state: IAppState) => {
		if (state?.error) {
			return state.error;
		}
	}
);

export const getIsCommissioner = createSelector(
	getAppState,
	(state: IAppState) => {
		if (state?.isCommissioner) {
			return state.isCommissioner;
		}
	}
);

export const getSelectedWeek = createSelector(
	getAppState,
	(state: IAppState) => {
		if (state?.selectedWeek) {
			return state.selectedWeek;
		}
	}
);

export const getAllFighters = createSelector(
	getAppState,
	(state: IAppState) => {
		if (state?.allFighters) {
			return state.allFighters;
		}
	}
);

export const getEventsList = createSelector(
	getAppState,
	(state: IAppState) => {
		if (state?.events) {
			return state.events;
		}
	}
);
