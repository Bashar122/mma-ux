export interface ICompareFighter {
	name: string;
	record: string;
	fighterHistory: any[];
	nickname: string;
}

export interface ICompareFighterModal {
	open: boolean;
	fighter1: ICompareFighter;
	fighter2: ICompareFighter;
}
