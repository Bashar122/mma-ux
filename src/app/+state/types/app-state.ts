export enum SCORING_SYSTEM_ENUM {
	CONFIDENCE = 'confidence',
	METHOD = 'method',
}

export interface IMethodPick {
	round?: string;
	finish?: string;
	winner?: string;
}

export interface IMethod {
	picks: IMethodPick[];
	totalPoints: number;
}

export interface IConfidencePick {
	winner: string;
	confidence: number;
}

export interface IConfidence {
	picks: IConfidencePick[];
	totalPoints: number;
}

export interface IScoring {
	type: SCORING_SYSTEM_ENUM;
	system: IConfidence | IMethod;
}

export interface ILocation {
	city: string;
	state: string;
	zip: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IStats {

}

export interface IPayout {
	first?: string;
	second?: string;
	third?: string;
	fourth?: string;
	fifth?: string;
}

export interface ILeagueConfiguration {
	numberOfMembers: number;
	payout: IPayout;
	start: string;
	end: string;
	type: string;
}

export interface IUserInfo {
	aboutMe: string[];
	address: Location;
	created?: string;
	dob: string;
	email: string;
	enabled?: boolean;
	firstName: string;
	id: string;
	language: string;
	lastName: string;
	leagues: ILeague[];
	permissions?: string;
	phone: any;
	pic: string;
	record: string;
	stats: IStats;
}

export interface IFight {
	fighter1Id: string;
	fighter2Id: string;
	rounds: number;
	weight: number;
	championship: boolean;
	bonus?: IBonus[];
}

export interface IBonus {
	amount: number;
	recipient: string;
}

export interface IPick {
	name?: string;
	fight?: IFight;
	winner: string;
	method?: string;
	round?: string;
	confidence?: number;
	matchPoint?: number;
	potentialScore?: number;
	bonuses?: IBonus[];
}

export interface ISelections {
	opponent: string;
	event: string;
	picks: IPick[];
	totalPoints: number;
}

export interface IRecord {
	wins: number;
	losses: number;
	ties: number;
}

export interface IMember {
	id: any;
	teamName: string;
	record: IRecord;
	picks: ISelections[];
	pic: string;
	totalPoints: number;
}

export interface IRules {
	numberOfTeam: number;
	private: boolean;
	scoring: IScoring;
}

export interface IPlayer {
	name: string;
	id: string;
	nickName: string;
	totalPoints: string;
}

export interface IMatchUp {
	playerOne: IPlayer;
	playerTwo: IPlayer;
	winner: IPlayer;
	loser: IPlayer;
	score: number;
}

export interface IWeeks {
	matchUps: IMatchUp[];
}

export interface ILeague {
	configuration: ILeagueConfiguration;
	end: string;
	id: string;
	name: string;
	results: IWeeks[];
	rules: IRules;
	start: string;
	teams: IMember[];
}

export interface IEvent {
	eventTitle?: string;
	venue?: string;
	date?: string;
	lineup?: any[];
	id: string;
	started: boolean;
	completed: boolean;
	scoringType: string;
}

