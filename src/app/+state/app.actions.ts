import { createAction } from '@ngrx/store';
import { ICredentials } from './types/login';
import { ILeague, IMember, IEvent } from './types/app-state';

export const appState = createAction('[App] App State');

export const verifyCredentials = createAction('[App] Verify Credentials');

export const login = createAction(
	'[App] Login',
	(credentials: ICredentials) => credentials,
);

export const setUserInfo = createAction(
	'[App] Set User Info',
	(userInfo: any) => userInfo,
);

export const getUserInfo = createAction(
	'[App] Get User Info',
	(id: string) => ({ id }),
);

export const setTeam = createAction(
	'[App] Set Team',
	(team: IMember) => ({ team }),
);

export const fetchLeague = createAction(
	'[App] Get League',
	(leagueId: string) => ({ leagueId }),
);

export const getLeagueByTeam = createAction(
	'[app] Get League By Team',
	(teamId: string) => ({ teamId }),
);

export const setLeague = createAction(
	'[App] Set League',
	(league: ILeague, isCommissioner: boolean) => ({ league, isCommissioner }),
);

export const getEvent = createAction(
	'[App] Get Event',
	(eventId: string) => ({ eventId }),
);

export const setEvent = createAction(
	'[App] Set Event',
	(event: any) => event,
);

export const udpateTeam = createAction(
	'[App] Update Team',
	(team: IMember) => ({ team }),
);

export const updateEvent = createAction(
	'[App] Update Event',
	(event: IEvent, teams: IMember[]) => ({ event, teams }),
);

export const printError = createAction(
	'[App] Print Error',
	(error: string) => ({ error }),
);

export const setSelectedWeek = createAction(
	'[App Set Selected Week',
	(selectedWeek: number) => ({ selectedWeek }),
);

export const updatePassword = createAction(
	'[App Update Password]',
	(password: string) => ({ password }),
);

export const getNewFighter = createAction(
	'[App Get New Fighter]',
	(path: string) => ({ path }),
);

export const fetchAllFighters = createAction(
	'[App Fetch All Fighters]',
);

export const setAllFighters = createAction(
	'[App Set All Fighters]',
	(allFighters: any[]) => ({ allFighters }),
);

export const createNewEvent = createAction(
	'[App Create New Event]',
	(event: IEvent, league: ILeague) => ({ event, league }),
);

export const retrieveEventsList = createAction(
	'[App Retrieve Events List',
	(year: string) => ({ year }),
);

export const setEventsList = createAction(
	'[App Set Events lists]',
	(events: any[]) => ({ events }),
);

export const addMatchups = createAction(
	'[App Add Match-Ups',
	(matchups: any[], league: ILeague, event: string) => ({ 
		matchups,
		league,
		event,
	}),
);

export const reset = createAction(
	'[App Reset]',
	() => ({ }),
);