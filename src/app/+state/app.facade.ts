import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as AppAction from './app.actions';
import { IAppPartialState } from './app.reducer';
import * as AppTypes from './types/app-state';
import * as AppQuery from './app.selectors';
import { ICredentials } from './types/login';

@Injectable()
export class AppFacade {
	public getError$ = this.store.pipe(select(AppQuery.getError));
	public getEvent$ = this.store.pipe(select(AppQuery.getEvent));
	public getLeague$ = this.store.pipe(select(AppQuery.getLeague));
	public getSelectedTeam$ = this.store.pipe(select(AppQuery.getSelectedTeam));
	public getTeams$ = this.store.pipe(select(AppQuery.getTeams));
	public getUserInfo$ = this.store.pipe(select(AppQuery.getUserInfo));
	public isCommissioner$ = this.store.pipe(select(AppQuery.getIsCommissioner));
	public getSelectedWeek$ = this.store.pipe(select(AppQuery.getSelectedWeek));
	public getAllFighters$ = this.store.pipe(select(AppQuery.getAllFighters));
	public getEventsList$ = this.store.pipe(select(AppQuery.getEventsList));

	constructor(private store: Store<IAppPartialState>) { }

	public verifyCredentials(): void {
		this.store.dispatch(AppAction.verifyCredentials());
	}

	public login(credentials: ICredentials): void {
		this.store.dispatch(AppAction.login(credentials));
	}

	public getUserInfo(id: string): void {
		this.store.dispatch(AppAction.getUserInfo(id));
	}

	public setUserInfo(userInfo: AppTypes.IUserInfo): void {
		this.store.dispatch(AppAction.setUserInfo(userInfo));
	}

	public setLeague(league: AppTypes.ILeague, isCommissioner: boolean): void {
		this.store.dispatch(AppAction.setLeague(league, isCommissioner));
	}

	public fetchLeague(leagueId: string): void {
		this.store.dispatch(AppAction.fetchLeague(leagueId));
	}

	public getLeagueByTeam(teamId: string): void {
		this.store.dispatch(AppAction.getLeagueByTeam(teamId));
	}

	public setTeam(team: AppTypes.IMember): void {
		this.store.dispatch(AppAction.setTeam(team));
	}

	public getEvent(eventId: string): void {
		this.store.dispatch(AppAction.getEvent(eventId));
	}

	public udpateTeam(team: AppTypes.IMember): void {
		this.store.dispatch(AppAction.udpateTeam(team));
	}

	public updateEvent(event: AppTypes.IEvent, teams: AppTypes.IMember[]): void {
		this.store.dispatch(AppAction.updateEvent(event, teams));
	}

	public setEvent(event: AppTypes.IEvent | null): void {
		this.store.dispatch(AppAction.setEvent(event));
	}

	public setSelectedWeek(selectedWeek: number): void {
		this.store.dispatch(AppAction.setSelectedWeek(selectedWeek));
	}

	public updatePassword(password: string): void {
		this.store.dispatch(AppAction.updatePassword(password));
	}

	public setError(error: string | null): void {
		this.store.dispatch(AppAction.printError(error));
	}

	public getNewFighter(path: string): void {
		this.store.dispatch(AppAction.getNewFighter(path));
	}

	public fetchAllFighters(): void {
		this.store.dispatch(AppAction.fetchAllFighters());
	}

	public createNewEvent(event: AppTypes.IEvent, league: AppTypes.ILeague): void {
		this.store.dispatch(AppAction.createNewEvent(event, league));
	}

	public retrieveEventsLists(year: string): void {
		this.store.dispatch(AppAction.retrieveEventsList(year));
	}

	public addMatchups(matchups: any[], league: AppTypes.ILeague, event: string): void {
		this.store.dispatch(AppAction.addMatchups(matchups, league, event));
	}

	public resetApp(): void {
		this.store.dispatch(AppAction.reset());
	}
}
