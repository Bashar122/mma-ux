import { Component, OnDestroy } from '@angular/core';
import { AppFacade } from './+state/app.facade';
import { Subscription } from 'rxjs';
import { IUserInfo } from './+state/types';

@Component({
	selector: 'app-root',
	styleUrls: ['./app.component.scss'],
	templateUrl: './app.component.html',
})
export class AppComponent implements OnDestroy {
	public loginToggle: boolean = true;
	private $getUserInfo: Subscription;

	constructor(
		private appFacade: AppFacade,
	) {
		this.$getUserInfo = this.appFacade.getUserInfo$.subscribe((userInfo: IUserInfo) => {
			if (userInfo) {
				this.loginToggle = false;
			} else {
				this.loginToggle = true;
			}
		});
	}

	public ngOnDestroy(): void {
		this.$getUserInfo?.unsubscribe();
	}
}
