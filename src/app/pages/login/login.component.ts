import { Component, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppFacade } from 'src/app/+state/app.facade';
import { Subscription } from 'rxjs';
import { LoginService } from '../../services/login-service';

@Component({
	animations: [
		trigger('dialog', [
			transition('void => *', [
				style({ transform: 'scale3d(.3, .3, .3)' }),
				animate(100)
			]),
			transition('* => void', [
				animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
			])
		]),
	],
	selector: 'login',
	styleUrls: ['./login.component.scss'],
	templateUrl: './login.component.html',
})
export class LoginComponent implements OnDestroy {
	@Input()
	public closable = true;
	@Input()
	public visible: boolean;

	@Output()
	public visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

	public form: FormGroup;
	public error: string;
	public isLoggedIn: boolean = false;

	private getLoggedInUser$: Subscription;
	private getError$: Subscription;
	private getVerifiedUser$: Subscription;

	constructor(
		private fb: FormBuilder,
		private appFacade: AppFacade,
		private loginService: LoginService,
	) {
		this.createForm();

		this.getError$ = this.appFacade.getError$.subscribe((error: string) => {
			if (error) {
				this.error = error;
			}
		});

		this.getLoggedInUser$ = this.appFacade.getUserInfo$.subscribe((loggedInUser) => {
			// TODO: [**LOGGING**] Replace With Logging
			if (loggedInUser) {
				console.log(`${loggedInUser.firstName} ${loggedInUser.lastName} has logged in. `);
				this.visible = false;
			}
		});
	}

	public createForm(): void {
		this.form = this.fb.group({
			password: ['', Validators.required],
			username: ['', Validators.required],
		});
	}

	public close(): void {
		this.visible = false;
		this.visibleChange.emit(this.visible);
	}

	public login(): void {
		this.appFacade.login({
			password: this.form.get('password').value,
			username: this.form.get('username').value.toLowerCase(),
		});
	}

	public ngOnDestroy(): void {
		if (this.getLoggedInUser$) {
			this.getLoggedInUser$.unsubscribe();
		}

		if (this.getError$) {
			this.getError$.unsubscribe();
		}

		if (this.getVerifiedUser$) {
			this.getVerifiedUser$.unsubscribe();
		}
	}
}
