import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppFacade } from 'src/app/+state/app.facade';
import { IUserInfo, IMember } from 'src/app/+state/types/app-state';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-home-page',
	styleUrls: ['./home-page.component.scss'],
	templateUrl: './home-page.component.html',
})
export class HomePageComponent implements OnDestroy, OnInit {
	public userInfo: IUserInfo;

	private getUserInfo$: Subscription;

	constructor(
		private appFacade: AppFacade,
		private router: Router,
	) {
		this.getUserInfo$ = this.appFacade.getUserInfo$.subscribe((userInfo: IUserInfo) => {
			this.userInfo = userInfo;
		});
	}

	public ngOnInit(): void {
		this.appFacade.setTeam(null);
		this.appFacade.setEvent(null);
		// this.appFacade.setLeague({} as ILeague, false);
	}

	public goToTeam(team: IMember): void {
		// if (team.picks.length === 0) {
		// 	return;
		// }

		this.appFacade.getLeagueByTeam(team.id);
		this.appFacade.setTeam(team);
		this.appFacade.setSelectedWeek(team.picks.length);
		this.appFacade.getEvent(team.picks[team.picks.length - 1]?.event);
		this.router.navigateByUrl('team-page');
	}


	public ngOnDestroy(): void {
		if (this.getUserInfo$) {
			this.getUserInfo$.unsubscribe();
		}
	}
}
