import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppFacade } from 'src/app/+state/app.facade';
import { IMember, IEvent } from 'src/app/+state/types/app-state';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LeagueService } from '../../services/league-service';

@Component({
	selector: 'commissioner-tools',
	styleUrls: ['commissioner-tools.component.scss'],
	templateUrl: 'commissioner-tools.component.html'
})
export class CommissionerToolsComponent implements OnDestroy {
	public team: IMember;
	public selectedWeek: number;
	public event: IEvent;
	public eventForm: FormGroup = this.fb.group({});
	public isUpdating: boolean = true;
	public popupText: string = 'Loading...';

	private teams: IMember[];
	private getTeam$: Subscription;
	private getEvent$: Subscription;
	private getTeams$: Subscription;

	constructor(
		private fb: FormBuilder,
		private router: Router,
		private appFacade: AppFacade,
		private activatedRoute: ActivatedRoute,
		private leagueServices: LeagueService,
	) {
		this.activatedRoute.queryParams.subscribe(params => {
			this.selectedWeek = params.selectedWeek;
		});

		this.getTeams$ = this.appFacade.getTeams$.subscribe((teams) => {
			if (teams) {
				this.teams = teams;
			}
		});

		this.getTeam$ = this.appFacade.getSelectedTeam$.subscribe(selectedTeam => {
			if (selectedTeam) {
				this.team = selectedTeam;
				this.selectedWeek = this.team.picks.length - 1;
				this.appFacade.getEvent(this.team.picks[this.selectedWeek]?.event);
			} else {
				this.router.navigateByUrl('home-page');
			}
		});

		this.getEvent$ = this.appFacade.getEvent$.subscribe(event => {
			if (event) {
				if (!event || !event.lineup) {
					this.isUpdating = false;
					return;
				}

				this.event = event;

				for (const entry of this.event.lineup.entries()) {
					// might have to use addControl if I need to update value and validity
					this.eventForm.registerControl(
						`fight${entry[0] + 1}`,
						this.fb.group({
							method: [null],
							rounds: [null],
							started: [null],
							winner: [null]
						})
					);

					this.eventForm.controls[`fight${entry[0] + 1}`].patchValue({
						method: this.event.lineup[this.event.lineup.length - 1 - entry[0]].method,
						rounds: this.event.lineup[this.event.lineup.length - 1 - entry[0]].round,
						started: this.event.lineup[this.event.lineup.length - 1 - entry[0]].started,
						winner: this.event.lineup[this.event.lineup.length - 1 - entry[0]].winner
					});
				}

				if (this.eventForm.dirty) {
					this.eventForm.reset(this.eventForm.value);
				}

				this.isUpdating = false;
			}
		});
	}

	public updateEvent(): void {
		this.popupText = 'Updating...';
		this.isUpdating = true;

		let fightCounter = this.event.lineup.length;

		for (const entries of this.event.lineup.entries()) {
			this.event.lineup[entries[0]].winner = this.eventForm.controls[`fight${fightCounter}`].value.winner;
			this.event.lineup[entries[0]].method = this.eventForm.controls[`fight${fightCounter}`].value.method;
			this.event.lineup[entries[0]].round = this.eventForm.controls[`fight${fightCounter}`].value.rounds;
			this.event.lineup[entries[0]].started = this.eventForm.controls[`fight${fightCounter}`].value.started;
			fightCounter--;
		}

		if (!this.event.started) {
			this.event.started = true;
		}
		
		this.appFacade.updateEvent(this.event, this.teams);
	}

	public checkRounds(rounds: number): any[] {
		const roundsArray = [
			{ display: '', value: null },
			{ display: 'First', value: '1' },
			{ display: 'Second', value: '2' },
			{ display: 'Third', value: '3' }
		];

		if (rounds === 3) {
			return roundsArray;
		}

		roundsArray.push({ display: 'Four', value: '4' });
		roundsArray.push({ display: 'Five', value: '5' });

		return roundsArray;
	}

	public ngOnDestroy(): void {
		if (this.getTeam$) {
			this.getTeam$.unsubscribe();
		}

		if (this.getEvent$) {
			this.getEvent$.unsubscribe();
		}

		if (this.getTeams$) {
			this.getTeams$.unsubscribe();
		}
	}
}
