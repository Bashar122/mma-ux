import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { trigger, style, animate, transition } from '@angular/animations';
import { AppFacade } from 'src/app/+state/app.facade';
import { Router } from '@angular/router';
import { IMember, IEvent } from 'src/app/+state/types/app-state';
import { ScoringUtil } from 'src/app/helpers/scoring-util';
import { Subscription } from 'rxjs';
import { get } from 'lodash';
import { ICompareFighter, ICompareFighterModal } from '../../+state/types';
import * as AppTypes from '../../+state/types/app-state';
import { SCORING_SYSTEM_ENUM } from '../../+state/types/app-state';

@Component({
	animations: [
		trigger('dialog', [
			transition('void => *', [style({ transform: 'scale3d(.3, .3, .3)' }), animate(100)]),
			transition('* => void', [animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))])
		])
	],
	selector: 'team-page',
	styleUrls: ['./team-page.component.scss'],
	templateUrl: './team-page.component.html'
})
export class TeamPageComponent implements OnDestroy, OnInit {

	// Public Variables
	public compareFighterModal: ICompareFighterModal = {
		fighter1: {} as ICompareFighter,
		fighter2: {} as ICompareFighter,
		open: false
	};
	public allTeams: IMember[] = [];
	
	public confidenceFlag: boolean = false;
	public isLoading: boolean = false;
	public selectedWeek: number;
	public potentialScore: number = 0;
	
	public picks: FormGroup = this.fb.group({});
	public selectedEventsFg: FormGroup = this.fb.group({
		selectedEvents: [''],
	});

	public event: IEvent;
	public team: IMember;

	public opponentInfo: any;
	public numberOfEvents: any[] = [];
	public scoringTypeEnum = SCORING_SYSTEM_ENUM;
	public confidenceArray = [];

	// Private Variables
	private getEvent$: Subscription;
	private getTeam$: Subscription;
	private getTeams$: Subscription;
	private getSelectedWeek$: Subscription;

	constructor(
		private appFacade: AppFacade,
		private router: Router,
		private fb: FormBuilder,
		private scoringUtil: ScoringUtil,
	) {}

	public ngOnInit(): void {	
		this.getTeam$ = this.appFacade.getSelectedTeam$.subscribe((selectedTeam: AppTypes.IMember) => {
			if (selectedTeam) {
				this.isLoading = !this.team ? true : false;
				this.team = selectedTeam;
				// this.selectedWeek = selectedTeam.picks.length;

				for (const entries of selectedTeam.picks.entries()) {
					this.numberOfEvents.push({ 
						display: `Week ${entries[0] + 1}`, 
						value: entries[0] + 1,
					});
				}

				this.selectedEventsFg.controls['selectedEvents'].patchValue(selectedTeam.picks.length);

				if (this.picks.dirty) {
					this.picks.reset(this.picks.value);
				}
			} else {
				this.router.navigateByUrl('home-page');
			}
		});

		this.getSelectedWeek$ = this.appFacade.getSelectedWeek$.subscribe((selectedWeek: number) => {
			if (selectedWeek) {
				this.selectedWeek = selectedWeek;
				this.selectedEventsFg.controls['selectedEvents'].patchValue(this.selectedWeek);
			} else if (this.team) {
				this.appFacade.setSelectedWeek(this.team.picks.length);
			}
		});

		this.getEvent$ = this.appFacade.getEvent$.subscribe(event => {
			if (event) {
				this.event = event;

				if (this.event.scoringType === this.scoringTypeEnum.CONFIDENCE) {
					this.confidenceFlag = true;
					this.generateConfidenceArray();
				} else {
					this.confidenceFlag = false;
				}

				this.buildForm();
				this.isLoading = false;
			}
		});

		this.getTeams$ = this.appFacade.getTeams$.subscribe((allTeams: IMember[]) => {
			if (allTeams) {
				this.allTeams = allTeams;
				this.opponentInfo = this.getOpponentInfo(this.team.picks[this.selectedWeek - 1]?.opponent, this.allTeams);
			}
		});
	}

	public changeWeeks(): void {
		const newlySelectedWeek: number = +this.selectedEventsFg.value.selectedEvents;
		this.isLoading = true;
		this.compareFighterModal.open = false;
		this.appFacade.setSelectedWeek(newlySelectedWeek);
		const eventId = this.team.picks[newlySelectedWeek - 1].event;
		this.appFacade.getEvent(eventId);
		this.opponentInfo = this.getOpponentInfo(this.team.picks[newlySelectedWeek - 1].opponent, this.allTeams);
	}

	public setPicks(): void {
		this.isLoading = true;
		this.appFacade.udpateTeam(this.team);
	}

	public getSelectedFighterName(fighterId: string, index: number): string {
		const fight = get(this.event, `lineup[${index}].fight`, null);

		if (!fight) {
			return null;
		}

		let fighterName: string;

		switch (fighterId) {
			case fight.fighter1.id:
				fighterName = fight.fighter1.name;
				break;
			case 'draw':
				fighterName = 'draw';
				break;
			default: 
				fighterName = fight.fighter2.name;
		}

		return fighterName;
	}

	public compareFighters(fighter1, fighter2): void {
		this.compareFighterModal.fighter1 = fighter1;
		this.compareFighterModal.fighter2 = fighter2;
		this.compareFighterModal.open = !this.compareFighterModal.open;
	}

	public getOpponentInfo(opponentId: string, allTeams: IMember[]): any { 
		for (const team of allTeams) {
			if (team.id === opponentId) {
				return {
					record: team.record,
					teamName: team.teamName,
					totalPoints: team.totalPoints,
				};
			}
		}
	}

	public ngOnDestroy(): void {
		const subscriptionArray: string[] = [
			'getTeam$',
			'getEvent$',
			'getTeams$',
			'getSelectedWeek$',
		];

		for (const subscription of subscriptionArray) {
			if (this[subscription]) {
				this[subscription].unsubscribe();
			}
		}
	}

	public clearConfidence(index: number): void {
		if (!this.event.lineup[index].started) {
			const confidence = this.picks.controls['fight' + (index + 1)].value['confidence'];
			this.confidenceArray[confidence].hide = false;
			this.picks.controls['fight' + (index + 1)].setValue({
				winner: '',
				confidence: '',
			});
			this.preparePicks();
		}
	}

	public preparePicks(): void {
		this.potentialScore = 0;
		this.generateConfidenceArray();
		const selections = this.team.picks[this.selectedWeek - 1].picks.map(
			(pick, index) => {
				this.potentialScore += this.scoringUtil.potentialScore(
					this.picks.value[`fight${index + 1}`], 
					this.event.lineup[index].fight,
					this.event.scoringType,
				);

				const winner = this.picks.value[`fight${index + 1}`]?.winner;
				const method = this.picks.value[`fight${index + 1}`]?.method;
				const rounds = this.picks.value[`fight${index + 1}`]?.rounds;

				if (this.confidenceFlag) {
					const confidence: number = parseInt(this.picks.controls[`fight${index + 1}`].value['confidence']);

					if (confidence > 0) {
						this.confidenceArray[confidence].hide = true;
					} else if (confidence) {
						this.confidenceArray[confidence].hide = false;
					}

					return {
						winner: winner,
						confidence: confidence,
					};
				}
				
				return {
					method: method,
					round: rounds,
					winner: winner,
				};
			}
		);

		this.team.picks[this.selectedWeek - 1].picks = selections;
	}

	private buildForm(): void {
		if (!this.event || !this.event.lineup) {
			return;
		}

		this.picks = this.fb.group({});

		for (const entries of this.event.lineup.entries()) {
			const selection = this.team.picks[this.selectedWeek - 1].picks[entries[0]];
			let newFormGroup: FormGroup = this.fb.group({
				method: ['', [Validators.required]],
				rounds: ['', [Validators.required]],
				winner: ['', [Validators.required]],
			});

			if (this.confidenceFlag) {
				newFormGroup = this.fb.group({
					winner: ['', [Validators.required]],
					confidence: ['', [Validators.required]],
				});
			}

			this.picks.registerControl(`fight${entries[0] + 1}`, newFormGroup);

			// picks are already made, then prepop
			if (selection.winner) {
				let newPrepopFormGroup: any = {
					method: selection.method,
					rounds: selection.round,
					winner: selection.winner,
				};

				if (this.confidenceFlag) {
					newPrepopFormGroup = {
						winner: selection.winner,
						confidence: selection.confidence,
					};
				}

				this.picks.controls['fight' + (entries[0] + 1)].patchValue(newPrepopFormGroup);
			}
		}

		this.preparePicks();
	}

	private generateConfidenceArray(): void {
		this.confidenceArray = [{ 
			display: 'Select Confidence Level',
			value: '',
			hide: false,
		}];

		for (const [index] of this.event.lineup.entries()) {
			this.confidenceArray.push({ display: index + 1, value: index + 1, hide: false });
		}
	}
}
