import { Component, OnDestroy } from '@angular/core';
import { AppFacade } from 'src/app/+state/app.facade';
import { ILeague, IMember } from 'src/app/+state/types/app-state';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
	selector: 'league-home',
	styleUrls: ['./league-home.component.scss'],
	templateUrl: './league-home.component.html',
})
export class LeagueHomeComponent implements OnDestroy {
	public league: ILeague;
	public teams: IMember[];
	public selectedTeam: IMember;

	private getLeague$: Subscription;
	private getTeams$: Subscription;
	private getSelectedTeam$: Subscription;

	constructor(
		private appFacade: AppFacade,
		private router: Router,
	) {
		this.getSelectedTeam$ = this.appFacade.getSelectedTeam$.subscribe((selectedTeam: IMember) => {
			if ( selectedTeam ) {
				this.selectedTeam = selectedTeam;
			}
		});

		this.getLeague$ = this.appFacade.getLeague$.subscribe((league) => {
			if (league) {
				// TODO: [***Logging***] Replace With Logging
				console.log('league in league home: ', league);
				this.league = league;
			} else {
				this.router.navigateByUrl('home-page');
			}
		});

		this.getTeams$ = this.appFacade.getTeams$.subscribe((teams) => {
			if (teams) {
				this.teams = teams;
			}
		});
	}

	public selectTeam(team): void {
		this.appFacade.setTeam(team);
		this.appFacade.setLeague(null, false);
		this.router.navigateByUrl('team-page');
	}

	public ngOnDestroy(): void {
		if (this.getLeague$) {
			this.getLeague$.unsubscribe();
		}

		if (this.getTeams$) {
			this.getLeague$.unsubscribe();
		}

		if ( this.getSelectedTeam$ ) {
			this.getSelectedTeam$.unsubscribe();
		}
	}
}
