import { Component, OnDestroy } from '@angular/core';
import { AppFacade } from 'src/app/+state/app.facade';
import { IMember, ISelections, SCORING_SYSTEM_ENUM } from 'src/app/+state/types/app-state';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IEvent } from '../../+state/types/app-state';
import { ScoringUtil } from '../../helpers/scoring-util';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
	selector: 'match-up',
	templateUrl: 'match-up.component.html',
	styleUrls: ['match-up.component.scss']
})
export class MatchUpComponent implements OnDestroy {
	public selectedEventsFg: FormGroup = this.fb.group({
		selectedEvents: [''],
	});

	public selectedMatchUpFg: FormGroup= this.fb.group({
		selectedMatchUp: [''],
	});

	public numberOfEvents: any[] = [];
	public matchUpsList: any[] = [];
	public matchUpBoxScores: any[] = [];
	public allTeams: IMember[] = [];
	
	public selectedTeam: IMember;
	public team1: ISelections;
	public team2: ISelections;
	public team1Name: string;
	public team2Name: string;
	public selectedWeek: number;

	public isLoading: boolean = false;

	public event: IEvent;

	private getEvent$: Subscription;
	private getSelectedTeam$: Subscription;
	private getTeams$: Subscription;
	private selectedWeek$: Subscription;

	constructor(
		private appFacade: AppFacade, 
		private router: Router, 
		private scoringUtil: ScoringUtil,
		private fb: FormBuilder,
	) {
		this.isLoading = true;

		this.selectedWeek$ = this.appFacade.getSelectedWeek$.subscribe((selectedWeek: number) => {
			if (selectedWeek) {
				this.selectedWeek = selectedWeek;
				this.selectedEventsFg.controls['selectedEvents'].patchValue(this.selectedWeek);
			} else if (this.selectedTeam) {
				this.appFacade.setSelectedWeek(this.selectedTeam.picks.length);
			}
		});

		this.getTeams$ = this.appFacade.getTeams$.subscribe((teams: IMember[]) => {
			if (teams) {
				this.allTeams = teams;
			}
		});

		this.getEvent$ = this.appFacade.getEvent$.subscribe((event: IEvent) => {
			if (event) {
				this.isLoading = false;
				this.event = event;
				
				if (this.selectedTeam) {
					this.getMatchUps();
				}
			}
		});

		this.getSelectedTeam$ = this.appFacade.getSelectedTeam$.subscribe(
			(selectedTeam: IMember) => {
				if (selectedTeam) {
					this.selectedTeam = selectedTeam;
					this.getMatchUps();

					for (const [index] of selectedTeam.picks.entries()) {
						this.numberOfEvents.push({ 
							display: `Week ${index + 1}`, 
							value: index + 1,
						});
					}
				} else {
					this.router.navigateByUrl('/home-page');
				}
			},
		);
	}

	public changeMatchUp(): void {
		const selectedMatchupIds = this.selectedMatchUpFg.controls.selectedMatchUp.value.split(',');
		this.setMatchup(selectedMatchupIds);
	}

	public changeWeeks(): void {
		this.isLoading = true;
		this.event = null;
		this.appFacade.setSelectedWeek(this.selectedEventsFg.controls.selectedEvents.value);
		this.getMatchUps();
		const eventId = this.team1.event;
		this.appFacade.getEvent(eventId);
	}

	public getRdAbbreviation(round: string): string {
		switch (round) {
			case '1':
				return '1st';
			case '2': 
				return '2nd';
			case '3':
				return '3rd';
			case '4': 
				return '4th';
			case '5':
				return '5th';
		}
	}

	public getMatchUps(): void {
		const allTeamsCopy = [...this.allTeams];
		this.matchUpsList = [];

		for (const team of this.allTeams) {
			if (allTeamsCopy.includes(team)) {
				for(const [index, opponent] of allTeamsCopy.entries()) {
					if (team.picks[this.selectedWeek - 1 ]?.opponent === opponent?.id) {
						this.matchUpsList.push({ 
							display: `${team.teamName} vs ${opponent.teamName}`, 
							value: `${team.id},${opponent.id}`,
						});

						this.setEventDropdown(team, opponent);
						allTeamsCopy.splice(index, 1);
					}
				}
			}
		}
	}

	public ngOnDestroy(): void {
		const subscriptionArray: string[] = [
			'getEvent$',
			'getSelectedTeam$',
			'getTeams$',
		];

		for (const subscription of subscriptionArray) {
			if (this[subscription]) {
				this[subscription].unsubscribe();
			}
		}
	}

	private setEventDropdown(team, opponent): void {
		if (this.selectedTeam.id === team.id || this.selectedTeam.id === opponent.id) {
			this.selectedMatchUpFg
				.controls
				.selectedMatchUp
				.patchValue(
					this.matchUpsList[this.matchUpsList.length - 1].value,
				);

			this.setMatchup(this.matchUpsList[this.matchUpsList.length - 1].value.split(','));
		}
	}

	private setMatchup(selectedMatchupIds: string[]): void {
		this.team1 = null;
		this.team2 = null;
		let team2Id: string;

		for (const team of this.allTeams) {
			if (selectedMatchupIds[0] === team.id) {
				this.team1 = team?.picks[this.selectedWeek -  1];
				this.team1Name = team?.teamName;
			} else if (selectedMatchupIds[1] === team.id) {
				this.team2 = team?.picks[this.selectedWeek -  1];
				team2Id = team?.id;
				this.team2Name = team?.teamName;
			}

			if (this.team1 && this.team2) {
				break;
			}
		}

		if (this.selectedTeam.id === team2Id) {
			this.team2 = this.team1;
			this.team1 = this.selectedTeam.picks[this.selectedWeek - 1];
			this.team2Name = this.team1Name;
			this.team1Name = this.selectedTeam.teamName;
		}

		this.team1 = this.calculateScores(this.team1);
		this.team2 = this.calculateScores(this.team2);
	}

	private getSelectedFighterName(
		fighterId: string, 
		fightDetails: any
	): string {
		if (!fighterId || !fightDetails) {
			return null;
		}

		let fighterName: string;
		switch (fighterId) {
			case fightDetails.fighter1.id:
				fighterName = fightDetails.fighter1.name ;
				break;
			case 'draw':
				fighterName = 'draw';
				break;
			default: 
				fighterName = fightDetails.fighter2.name;
		}

		return fighterName;
	}

	private calculateScores(team: ISelections): ISelections {
		if (!this.event) {
			return team;
		}

		let actualScore = 0;
		let potentialScore = 0;

		for (const [index, result] of this.event.lineup.entries()) {
			if (result.winner) {
				actualScore = this.getActualScore(team.picks[index], result, this.event.scoringType);
				team.picks[index].bonuses = this.scoringUtil.getBonusPts(team.picks[index].winner, result.fight);
			}
			team.picks[index].name = this.getSelectedFighterName(team.picks[index].winner, result.fight);

			potentialScore = this.getPotentialScore(team.picks[index], result.fight, this.event.scoringType);

			// team.picks[index].round = this.getRdAbbreviation(team.picks[index].round);
			team.picks[index].matchPoint = actualScore;
			team.picks[index].potentialScore = potentialScore;
		}

		return team;
	}

	private getActualScore(pick, result, scoringType): number {
		if (!result?.winner) {
			return null;	
		}

		if (scoringType === SCORING_SYSTEM_ENUM.CONFIDENCE) {
			return this.scoringUtil.scoreConfidence(result, pick);
		}

		return this.scoringUtil.scoreMethod(result, pick);
	}

	private getPotentialScore(pick, fight, scoringType): number {
		return this.scoringUtil.potentialScore(pick, fight, scoringType);
	}
}
