import { Component, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppFacade } from 'src/app/+state/app.facade';
import { Subscription } from 'rxjs';
import { IUserInfo } from 'src/app/+state/types/app-state';
import { Router } from '@angular/router';

@Component({
	selector: 'user-settings',
	styleUrls: ['user-settings.component.scss'],
	templateUrl: 'user-settings.component.html',
})
export class UserSettingsComponent implements OnDestroy {
	public passwordReset: FormGroup = this.fb.group({
		newPassword: ['', [Validators.required]],
		verifyPassword: ['', [Validators.required]],
	});

	public error: string;
	public successMsg: string;
	public isLoading: boolean;

	private getSuccessMessage$: Subscription;
	private getUserInfo$: Subscription;

	constructor(
		private readonly fb: FormBuilder,
		private readonly appFacade: AppFacade,
		private readonly router: Router,
	) {
		this.getUserInfo$ = this.appFacade.getUserInfo$.subscribe((userInfo: IUserInfo) => {
			if (!userInfo) {
				this.router.navigateByUrl('home-page');
			}
		});

		this.getSuccessMessage$ = this.appFacade.getError$.subscribe((updateMsg: string) => {
			if (updateMsg === 'password updated successfully') {
				this.successMsg = updateMsg;
				this.isLoading = false;
				this.appFacade.setError(null);
			} else {
				this.error = updateMsg;
			}
		});
	}

	public updatePassword(): void  {
		this.successMsg = null;
		const { newPassword, verifyPassword } = this.passwordReset.value;
		if (newPassword != verifyPassword) {
			this.error = 'Passwords do not match';
		} else {
			this.isLoading = true;
			this.error = null;
			this.appFacade.updatePassword(newPassword);
		}
	}

	public ngOnDestroy(): void {
		if (this.getSuccessMessage$) {
			this.getSuccessMessage$.unsubscribe();
		}

		if (this.getUserInfo$) {
			this.getUserInfo$.unsubscribe();
		}
	}
}