import { Component, OnInit } from '@angular/core';
import { FightersService } from '../../services/fighters-service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
	selector: 'edit-fighter',
	styleUrls: ['edit-fighters.component.scss'],
	templateUrl: 'edit-fighters.component.html',
})
export class EditFightersComponent implements OnInit {
	public fighters;
	public selectedFighter: number;

	public fighterForm = this.fb.group({
		fights: this.fb.array([this.fb.group({
			date: ['', Validators.required],
			method: ['', Validators.required],
			name: ['', Validators.required],
			opponent: ['', Validators.required],
			referee: ['', Validators.required],
			result: ['', Validators.required],
			round: ['', Validators.required],
			time: ['', Validators.required],
			// url: ['', Validators.required],
		})]),
		id: ['', Validators.required],
	});

	constructor(
		private fs: FightersService,
		private fb: FormBuilder,
	) { }

	public ngOnInit(): void {
		this.fs.getAllFighters().subscribe((payload) => {
			this.fighters = payload.fighters;
		});
	}

	public selectFighter(): void {
		this.selectedFighter = this.fighterForm.get('id').value;
		this.fighterForm.get('id').setValue(this.fighters[this.selectedFighter].id);
	}

	public editFighter(): void {
		console.log(this.fighterForm.controls);
	}
}
