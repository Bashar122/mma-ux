import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { AppFacade } from 'src/app/+state/app.facade';
import { Subscription } from 'rxjs';
import { ILeague } from 'src/app/+state/types';

@Component({
	selector: 'create-event',
	styleUrls: ['create-event.component.scss'],
	templateUrl: 'create-event.component.html',
})
export class CreateEventComponent implements OnInit, OnDestroy {
	public newEvent: FormGroup = this.fb.group({
		eventTitle: ['', [Validators.required]],
		date: ['', [Validators.required]],
		started: [false, [Validators.required]],
		venue: ['', [Validators.required]],
		completed: [false, [Validators.required]],
		lineup: this.fb.array([]),
		scoringType: ['', [Validators.required]],
	});

	public newMatchups: FormGroup = this.fb.group({
		event: ['', [Validators.required]],
		matchups: this.fb.array([
			this.fb.group({
				team1: ['', Validators.required],
				team2: ['', Validators.required],
			}),
			this.fb.group({
				team1: ['', Validators.required],
				team2: ['', Validators.required],
			}),
			this.fb.group({
				team1: ['', Validators.required],
				team2: ['', Validators.required],
			}),
			this.fb.group({
				team1: ['', Validators.required],
				team2: ['', Validators.required],
			}),
			this.fb.group({
				team1: ['', Validators.required],
				team2: ['', Validators.required],
			}),
			this.fb.group({
				team1: ['', Validators.required],
				team2: ['', Validators.required],
			}),
			this.fb.group({
				team1: ['', Validators.required],
				team2: ['', Validators.required],
			}),
			this.fb.group({
				team1: ['', Validators.required],
				team2: ['', Validators.required],
			}),
		]),
	});

	public newFighter: FormGroup = this.fb.group({
		fighterPath: ['', Validators.required],
	});

	public get lineups(): AbstractControl[] {
		return (this.newEvent.get('lineup') as FormArray).controls;
	}

	public get matchups(): AbstractControl[] {
		return (this.newMatchups.get('matchups') as FormArray).controls;
	}

	public league: ILeague;
	public allFighters: any[] = [];
	public allTeams: any[] = [];
	public events: any[] = [];
	private getAllFighters$: Subscription;
	private getAllTeams$: Subscription;
	private getLeague$: Subscription;
	private getEventsList$: Subscription;

	constructor(
		private readonly appFacade: AppFacade,
		private readonly fb: FormBuilder,
	) {
		this.getAllFighters$ = this.appFacade.getAllFighters$.subscribe((allFighters: any[]) => {
			if (allFighters) {
				for (const fighter of allFighters) {
					this.allFighters.push({
						display: fighter.name,
						value: fighter.id,
					});
				}

				this.allFighters.push({
					display: 'both',
					value: 'both',
				});
			}
		});

		this.getAllTeams$ = this.appFacade.getTeams$.subscribe((allTeams) => {
			if (allTeams) {
				for (const team of allTeams) {
					this.allTeams.push({
						display: team.teamName,
						value: team.id,
					});
				}
			}
		});

		this.getLeague$ = this.appFacade.getLeague$.subscribe((league) => {
			if (league) {
				this.league = league;
			}
		});

		this.getEventsList$ = this.appFacade.getEventsList$.subscribe((events: any[]) => {
			if (events) {
				this.events = events.map((event) => {
					return { display: event.eventTitle, value: event.id };
				});

				this.events.push({
					display: '',
					value: null,
				});
			}
		});
	}

	public ngOnInit(): void {
		this.appFacade.fetchAllFighters();
		this.appFacade.retrieveEventsLists('2024');
	}

	public createEvent(): void {
		const newEvent = this.newEvent.value;
		this.appFacade.createNewEvent(newEvent, this.league);
	}

	public addMatch(): void {
		const control = this.newEvent.get('lineup') as FormArray;
		control.push(
			this.fb.group({
				card: ['Main Card', [Validators.required]],
				method: [null],
				round: [null],
				started: [false, [Validators.required]],
				winner: [null],
				fight: this.fb.group({
					weight: [0, Validators.required],
					fighter1Id: ['', Validators.required],
					fighter2Id: ['', Validators.required],
					rounds: [3, Validators.required],
					bonus: this.fb.array([]),
					championship: [false, Validators.required],
				})
			}),
		);
	}

	public removeMatch(index): void {
		const control = this.newEvent.get('lineup') as FormArray;
		control.removeAt(index);
	}

	public addBonus(parentIndex): void {
		const parentControl = this.newEvent.get('lineup') as FormArray;
		const control = parentControl.controls[parentIndex].get('fight').get('bonus') as FormArray;
		control.push(
			this.fb.group({
				recipient: ['both', Validators.required],
				amount: [50, Validators.required],
				reason: ['close fight', Validators.required],
			}),
		);

		console.log(this.newEvent, 'newEvent');
	}

	public removeBonus(parentIndex, bonusIndex): void {
		const parentControl = this.newEvent.get('lineup') as FormArray;
		const control = parentControl.controls[parentIndex].get('fight').get('bonus') as FormArray;
		control.removeAt(bonusIndex);
	}
	public addMatchup(): void {
		this.appFacade.addMatchups(
			this.newMatchups.value.matchups,
			this.league,
			this.newMatchups.value.event,
		);
	}

	public addFighter(): void {
		this.appFacade.getNewFighter(this.newFighter.get('fighterPath').value);
	}

	public ngOnDestroy(): void {
		if (this.getAllFighters$) {
			this.getAllFighters$.unsubscribe();
		}

		if (this.getAllTeams$) {
			this.getAllTeams$.unsubscribe();
		}

		if (this.getLeague$) {
			this.getLeague$.unsubscribe();
		}

		if (this.getEventsList$) {
			this.getEventsList$.unsubscribe();
		}
	}
}